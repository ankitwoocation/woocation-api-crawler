package com.woocation.event.model;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Performer extends CommonDTO{

	private String name;
	
	private String type;
	
}
