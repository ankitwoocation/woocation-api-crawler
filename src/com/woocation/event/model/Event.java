package com.woocation.event.model;

import java.util.List;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new event.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Event extends CommonDTO{

	/** The venue capacity. */
	private String venueCapacity;
	
	/** The full address. */
	private String fullAddress;
	
	/** The venue url. */
	private String venueUrl;
	
	/** The doc type. */
	private String docType;
	
	/** The concert date. */
	private String concertDate;
	
	/** The yyyy. */
	private String yyyy;
	
	/** The ldp. */
	private GeoLocation location;
	
	/** The artist url. */
	private String artistUrl;
	
	/** The event name. */
	private String eventName;
	
	/** The additional details. */
	private List<String> additionalDetails;
	
	/** The pricing. */
	private List<Price> pricing;

	/** The address. */
	private Address address;
}
