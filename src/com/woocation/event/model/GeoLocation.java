package com.woocation.event.model;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * The Class Location.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public final class GeoLocation extends CommonDTO {

	/** The lat. */
	private Double latitude;

	/** The lon. */
	private Double longitude;
	
	/**
	 * Instantiates a new geo location.
	 *
	 * @param latitude the latitude
	 * @param longitude the longitude
	 */
	public GeoLocation(Double latitude, Double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}

}
