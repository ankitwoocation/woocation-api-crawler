package com.woocation.event.model;

import java.util.List;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The Class ResearchPaper.
 *
 * @author ankit.gupta
 */

@Data
@EqualsAndHashCode(callSuper = false)
public class ResearchPaper extends CommonDTO{

	/** The arxiv id. */
	private String arxivId;

	/** The doi. */
	private String doi;

	/** The description. */
	private String description;
	
	/** The title. */
	private String title;
	
	/** The url. */
	private String url;
	
	/** The dateline. */
	private String dateline;
	
	/** The journal reference. */
	private String journalReference;
	
	/** The submission history. */
	private String submissionHistory;
	
	/** The comments. */
	private String comments;
	
	/** The yyyy. */
	private String yyyy;
	
	/** The mm. */
	private String mm;
	
	/** The subjects. */
	private List<String> subjects;
	
	/** The dd. */
	private String dd;

	/** The report number. */
	private String reportNumber;
	
	/** The authors. */
	private List<String> authors;
	
	/** The pdf. */
	private String pdf;

	/** The archive year. */
	private String archiveYear;

	/** The research id. */
	private String researchId;
	
}
