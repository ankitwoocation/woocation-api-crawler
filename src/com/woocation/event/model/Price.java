package com.woocation.event.model;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Price extends CommonDTO{

	private String currency;
	
	private String price;
	
	private String vendor;
	
}
