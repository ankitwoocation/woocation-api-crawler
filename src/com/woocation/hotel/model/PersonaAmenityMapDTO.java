package com.woocation.hotel.model;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The Class PersonaDTO.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PersonaAmenityMapDTO extends CommonDTO {

    /** The amenity label. */
    private String personaId;

    /** The amenity card info. */
    private String amenityId;
    
    /** The amenity label. */
    private String amenityLabel;

    /** The persona description. */
    private int rank;
}
