package com.woocation.hotel.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * The Class AmenityDTO.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class AmenityDTO {

	/** The amenity id. */
	private String amenityId;
	
	/** The amenity label. */
	private String amenityLabel;

	/** The amenity card info. */
	private String amenityAlternateLabel;

	/** The category. */
	private String category;
	
	/** The default ranking. */
	private Integer defaultRanking;

	/** The properties. */
	private List<String> properties;
}
