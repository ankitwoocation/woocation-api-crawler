package com.woocation.hotel.model;

import java.util.List;

import com.google.common.base.Splitter;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Instantiates a new persona line.
 */
@Data
@NoArgsConstructor
public class PersonaLine {

	/** The amenity id. */
	private String amenityId;

	/** The amenity name. */
	private String amenityName;

	/** The alternate name. */
	private String alternateName;

	/** The category id. */
	private String categoryId;

	/** The category sub id. */
	private String categorySubId;

	/** The text. */
	private String text;

	/** The category name. */
	private String categoryName;

	/** The default rating. */
	private String defaultRating;

	/** The persona list. */
	private List<String> personaList;

	/**
	 * Persona builder.
	 *
	 * @param line
	 *            the line
	 * @return the persona line
	 */
	public static PersonaLine personaBuilder(String line) {
		List<String> values = Splitter.on(",").trimResults().splitToList(line);
		PersonaLine persona = new PersonaLine();
		persona.setAmenityId(values.get(0));
		persona.setAmenityName(values.get(1));
		persona.setAlternateName(values.get(2));
		persona.setCategoryId(values.get(3));
		persona.setCategorySubId(values.get(4));
		persona.setText(values.get(5));
		persona.setCategoryName(values.get(6));
		persona.setDefaultRating(values.get(7));
		persona.setPersonaList(values);
		return persona;
	}

}
