package com.woocation.hotel.model;

import java.util.ArrayList;
import java.util.List;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The Class PersonaDTO.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PersonaDTO extends CommonDTO {

    /** The amenity label. */
    private String personaId;

    /** The amenity card info. */
    private String personaLabel;

    /** The persona description. */
    private String personaDescription;

    /** The amenities persona. */
    private List<PersonaAmenityMapDTO> amenitiesPersona = new ArrayList<>();

}
