package com.woocation.hotel.model;

import java.util.ArrayList;
import java.util.List;

import com.woocation.model.Location;

import lombok.Data;

/**
 * Instantiates a new booking hotel.
 */
@Data
public class BookingHotel {

	/** The hotel id. */
	private Long hotelId;

	/** The hotel suggest. */
	private List<String> hotelSuggest;

	/** The name. */
	private String name;

	/** The property type. */
	private String propertyType;

	/** The location. */
	private Location location;

	/** The lattitude. */
	private Double latitude;

	/** The longitude. */
	private Double longitude;

	/** The city. */
	private String city;

	/** The country. */
	private String country;

	/** The street. */
	private String street;

	/** The locality. */
	private String locality;
	
	/** The languages spoken. */
	private List<String> languagesSpoken;
	
	/** The amenities id. */
	private List<Integer> amenitiesId;

	/**
	 * Sets the suggest.
	 */
	public void setSuggest() {
		List<String> suggestList = new ArrayList<>();
		suggestList.add(name);
		suggestList.add(name + " " + city);
		suggestList.add(name + " " + country);
		suggestList.add(name + " " + city + " " + country);
		this.setHotelSuggest(suggestList);
	}

}
