package com.woocation.main;

import java.io.File;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.woocation.model.AttractionRaw;
import com.woocation.model.User;

public class UserCrawler {

	public UserCrawler() {

		List<User> list = readFile("C:\\Users\\ankit.gupta\\Desktop\\Report_Data\\CREATE_Data.json", User.class);
		System.out.println(list);
		list.stream().forEach(item -> {
			System.out.println(item.getTimestamp() + "," + item.getAfter().getUserName());
		});

	}

	/**
	 * Read file.
	 *
	 * @param <T>
	 *            the generic type
	 * @param fileName
	 *            the file name
	 * @param pojoClass
	 *            the pojo class
	 * @return the list
	 */
	public <T> List<T> readFile(String fileName, Class<T> pojoClass) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		List<T> list = null;
		try {
			File jsonFile = new File(fileName);
			list = mapper.readValue(jsonFile, mapper.getTypeFactory().constructCollectionType(List.class, pojoClass));
			System.out.println("Records Read in File --> " + fileName + " --> " + list.size());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		UserCrawler crawler = new UserCrawler();
	}

}
