package com.woocation.witsy.github;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new github details.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GitArchieve extends CommonDTO{

	/** The id. */
	private Long id;
	
	private String type;
	
	private RepoDetails repo;
	
}
