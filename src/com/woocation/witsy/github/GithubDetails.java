package com.woocation.witsy.github;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new github details.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GithubDetails extends CommonDTO{

	/** The id. */
	private Long id;
	
	/** The name. */
	private String name;
	
	/** The full name. */
	private String full_name;
	
	/** The html url. */
	private String html_url;

	/** The description. */
	private String description;
	
	/** The owner. */
	private Owner owner;
	
	/** The avatar url. */
	private String avatar_url;
	
	/** The readme. */
	private String readme;
	
}
