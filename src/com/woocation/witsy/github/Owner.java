package com.woocation.witsy.github;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Owner extends CommonDTO{

	private String avatar_url;

}
