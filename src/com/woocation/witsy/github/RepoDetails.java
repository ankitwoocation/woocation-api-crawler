package com.woocation.witsy.github;

import com.woocation.model.CommonDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new github details.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RepoDetails extends CommonDTO{

	/** The id. */
	private Long id;
	
	/** The name. */
	private String name;
	
	/** The full name. */
	private String url;
	
}
