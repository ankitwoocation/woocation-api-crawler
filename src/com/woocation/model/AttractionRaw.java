package com.woocation.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new attraction raw.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class AttractionRaw extends CommonDTO{

	/** The id. */
	@JsonIgnore
	private Object _id;

	/** The geonameid. */
	private Long geonameid;
	
	/** The country. */
	private String country;
	
	/** The attractions. */
	private List<Attraction> attractions;
}
