package com.woocation.model;

import lombok.Data;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new admin 1 code.
 */
@Data
public class Admin1Code {

	/** The code. */
	private String code;
	
	/** The name. */
	private String name;
	
	/** The name 2. */
	private String name2;
	
	/** The population. */
	private String population;

	public Admin1Code(String code, String name, String name2, String population) {
		this.code = code;
		this.name = name;
		this.name2 = name2;
		this.population = population;
	}
}
