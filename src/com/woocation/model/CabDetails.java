package com.woocation.model;

import java.util.List;

import lombok.Data;

/**
 * Instantiates a new cab details.
 */
@Data
public class CabDetails {

	/** The geoname id. */
	private Long geonameId;
	
	/** The cabs. */
	private List<String> cabs;
}
