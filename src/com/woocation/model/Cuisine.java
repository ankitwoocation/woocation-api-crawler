package com.woocation.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new cuisine.
 */
@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cuisine {

	/** The id. */
	@JsonIgnore
	private Object _id;
	
	/** The geonameid. */
	private Long geonameid;
	
	/** The cuisines. */
	private List<List<String>> cuisines = null;
}
