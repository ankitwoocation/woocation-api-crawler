package com.woocation.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new user.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class User extends CommonDTO {

	/** The id. */
	private String _id;
	
	/** The timestamp. */
	private String timestamp;
	
	/** The after. */
	private After after;
	
}
