package com.woocation.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class After extends CommonDTO {

	private String userName;
	
}
