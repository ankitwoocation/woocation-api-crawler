package com.woocation.model;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new attraction.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Attraction extends CommonDTO {

	/** The attraction name. */
	private String attractionName;

	/** The categories. */
	private List<String> categories;

	/** The location. */
	private AttractionLocation location;

}
