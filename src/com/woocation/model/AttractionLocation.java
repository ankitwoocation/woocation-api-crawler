package com.woocation.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class AttractionLocation {

	@JsonIgnore
	private String type;
	
	/** The coordinates. */
	private List<Double> coordinates;

}
