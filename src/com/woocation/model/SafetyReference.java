package com.woocation.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The Class SafetyReference.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SafetyReference {

	/** The geonameid. */
	private Long geonameid;

	/** The city. */
	private String city;

	/** The attacks. */
	private Map<Integer, Integer> attacks;

	/** The safety score. */
	private String safetyScore;

	/**
	 * Gets the geonameid.
	 *
	 * @return the geonameid
	 */
	public Long getGeonameid() {
		return geonameid;
	}

	/**
	 * Sets the geonameid.
	 *
	 * @param geonameid
	 *            the new geonameid
	 */
	public void setGeonameid(Long geonameid) {
		this.geonameid = geonameid;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city
	 *            the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the attacks.
	 *
	 * @return the attacks
	 */
	public Map<Integer, Integer> getAttacks() {
		return attacks;
	}

	/**
	 * Sets the attacks.
	 *
	 * @param attacks
	 *            the attacks
	 */
	public void setAttacks(Map<Integer, Integer> attacks) {
		this.attacks = attacks;
	}

	/**
	 * Gets the safety score.
	 *
	 * @return the safety score
	 */
	public String getSafetyScore() {
		return safetyScore;
	}

	/**
	 * Sets the safety score.
	 *
	 * @param safetyScore
	 *            the new safety score
	 */
	public void setSafetyScore(String safetyScore) {
		this.safetyScore = safetyScore;
	}
}
