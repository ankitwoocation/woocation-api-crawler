/**
 * 
 */
package com.woocation.crawler;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import com.woocation.utils.EntityMapper;
import com.woocation.witsy.github.GitArchieve;
import com.woocation.witsy.github.GithubDetails;

/**
 * The Class GithubDataCrawler.
 *
 * @author Ankit.Gupta
 */
public class GithubDataCrawler {

	/** The executor. */
	private final ExecutorService executor = Executors.newFixedThreadPool(10);
	
	/** The Constant DIRECTORY. */
	public static final String DIRECTORY = "E:\\";

	/** The file path. */
	private String filePath = DIRECTORY + "Data\\github\\";

	/** The rest template. */
	private RestTemplate restTemplate;

	/**
	 * Instantiates a new github data crawler.
	 */
	public GithubDataCrawler() {
		initializeRestTemplate();
		try {
			List<GitArchieve> githubList = getData("2018-01-01-0.json");
			processGitArchieve(githubList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Process git archieve.
	 *
	 * @param githubList
	 *            the github list
	 * @throws Exception
	 */
	private void processGitArchieve(List<GitArchieve> githubList) throws Exception {
		List<GithubDetails> githubDetails = new ArrayList<>();
		githubList.stream().forEach(archieve -> {
			GithubDetails github = getGitHubRepository(archieve);
			if (github != null) {
				githubDetails.add(github);
			}
		});
		System.out.println(githubDetails);
	}
	
	private GithubDetails processOfflineData(GitArchieve archieve){
		GithubDetails details = new  GithubDetails();
//		details.setAvatar_url(archieve.getRepo().get);
		
		return details;
	}

	private GithubDetails getGitHubRepository(GitArchieve archieve) {
//		String url = "https://github.com/loysoft/node-oauth2-server/raw/master/README.md";//archieve.getRepo().getUrl();
		String url = archieve.getRepo().getUrl();
		
		GithubDetails githubDetail = null;
		try {
		String output = restTemplate.getForObject(url, String.class);
			githubDetail = EntityMapper.getInstance().getObject(output, GithubDetails.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return githubDetail;
	}

	/**
	 * Gets the data.
	 *
	 * @param fileName
	 *            the file name
	 * @return the data
	 * @throws Exception
	 *             the exception
	 */
	public List<GitArchieve> getData(String fileName) throws Exception {
		List<GitArchieve> githubList = new ArrayList<>();
		List<String> allLines = Files.readAllLines(Paths.get(filePath + fileName));
		allLines.stream().forEach(line -> {
			GitArchieve details;
			try {
				details = EntityMapper.getInstance().getObject(line, GitArchieve.class);
				githubList.add(details);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		return githubList;
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		GithubDataCrawler gs = new GithubDataCrawler();
	}

	private void initializeRestTemplate() {
		restTemplate = new RestTemplateBuilder().setConnectTimeout(5000).setReadTimeout(5000).build();
	}

}
