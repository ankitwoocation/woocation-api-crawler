package com.woocation.crawler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.woocation.event.model.PdfDTO;
import com.woocation.event.model.ResearchPaper;
import com.woocation.utils.EntityToDTO;

/**
 * The Class EventCrawler.
 */
public class ResearchPaperCrawler {

	/** The Constant DIRECTORY. */
	public static final String DIRECTORY = "E:\\";

	/** The file path. */
	private String filePath = DIRECTORY + "Data\\Witsy\\";
	
	private String fileExportPath = DIRECTORY + "Data\\Witsy\\PDF_Data";

	/** The event list. */
	private List<PdfDTO> eventList = new ArrayList<>();

	/**
	 * Instantiates a new research paper crawler.
	 */
	public ResearchPaperCrawler() {
		try {
			readPaperFile();
			// writeData();
			writePDFBean(fileExportPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read paper file.
	 */
	public void readPaperFile() {
		List<ResearchPaper> paperList = readFile(filePath + "arxiv.json", ResearchPaper.class);
		paperList.stream().forEach(paper -> {
			PdfDTO bean = EntityToDTO.getInstance().getObject(paper, ResearchPaper.class, PdfDTO.class);
			bean.setUrl(paper.getPdf());
			eventList.add(bean);
		});
	}

	/**
	 * Read file.
	 *
	 * @param <T>
	 *            the generic type
	 * @param fileName
	 *            the file name
	 * @param pojoClass
	 *            the pojo class
	 * @return the list
	 */
	public <T> List<T> readFile(String fileName, Class<T> pojoClass) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		List<T> list = null;
		try {
			File jsonFile = new File(fileName);
			list = mapper.readValue(jsonFile, mapper.getTypeFactory().constructCollectionType(List.class, pojoClass));
			System.out.println("Records Read in File --> " + fileName + " --> " + list.size());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

	/**
	 * Write data.
	 *
	 * @param count
	 *            the count
	 */
	private void writeData() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			File file = new File(filePath + "PDF_Paper" + ".json");
			file.createNewFile();
			mapper.writeValue(file, eventList);
			System.out.println("PDF exported to file --> " + file.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writePDFBean(String cityOutPutFolder) throws Exception {
		List<List<PdfDTO>> subSets = Lists.partition(eventList, 10000);
		int count = 1;
		for (List<PdfDTO> cityList : subSets) {
			ObjectMapper mapper = new ObjectMapper();
			File file = new File(cityOutPutFolder + count + ".json");
			file.createNewFile();
			mapper.writeValue(file, cityList);
			System.out.println("Data exported to file --> " + file.getAbsolutePath());
			count++;
		}
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		ResearchPaperCrawler paperCrawler = new ResearchPaperCrawler();
	}

}
