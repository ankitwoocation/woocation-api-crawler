/**
 * 
 */
package com.woocation.crawler;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.woocation.hotel.model.BookingHotel;
import com.woocation.model.Location;

/**
 * The Class WoocationHotelCrawler.
 *
 * @author Ankit.Gupta
 */
public class WoocationHotelCrawler {

	/** The hotel map. */
	private Map<Long, BookingHotel> hotelMap = new HashMap<>();

	/** The Constant DIRECTORY. */
	public static final String DIRECTORY = "E:\\";

	/** The hotel final. */
	private String hotelFinal = DIRECTORY + "Data\\New_Data\\Hotel_Data_Final\\";

	/**
	 * Instantiates a new woocation hotel crawler.
	 */
	public WoocationHotelCrawler() {
		readWeatherFile("E:\\Data\\New_Data\\");
		if (hotelMap.size() > 0) {
			try {
				writeHotelData(hotelFinal);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Read weather file.
	 *
	 * @param filePath
	 *            the file path
	 */
	public void readWeatherFile(String filePath) {
		try {
			List<File> filesInFolder = Files.walk(Paths.get(filePath + "Booking_hotel")).filter(Files::isRegularFile)
					.map(Path::toFile).collect(Collectors.toList());
			filesInFolder.stream().forEach(file -> {
				List<BookingHotel> weatherList = readFile(file.getAbsolutePath(), BookingHotel.class);
				weatherList.stream().forEach(item -> {
					Location location = new Location(item.getLatitude(), item.getLongitude());
					item.setLocation(location);
					hotelMap.put(Long.valueOf(item.getHotelId()), item);
				});
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read file.
	 *
	 * @param <T>
	 *            the generic type
	 * @param fileName
	 *            the file name
	 * @param pojoClass
	 *            the pojo class
	 * @return the list
	 */
	public <T> List<T> readFile(String fileName, Class<T> pojoClass) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		List<T> list = null;
		try {
			File jsonFile = new File(fileName);
			list = mapper.readValue(jsonFile, mapper.getTypeFactory().constructCollectionType(List.class, pojoClass));
			System.out.println("Records Read in File --> " + fileName + " --> " + list.size());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

	/**
	 * Write data.
	 */
	private void writeData() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			File file = new File(hotelFinal + ".json");
			file.createNewFile();
			mapper.writeValue(file, hotelMap.values());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write hotel data.
	 *
	 * @param cityOutPutFolder
	 *            the city out put folder
	 * @throws Exception
	 *             the exception
	 */
	private void writeHotelData(String cityOutPutFolder) throws Exception {
		List<BookingHotel> bookingList = hotelMap.values().stream().collect(Collectors.toList());
		List<List<BookingHotel>> subSets = Lists.partition(bookingList, 25000);
		int count = 1;
		for (List<BookingHotel> hotelList : subSets) {
			ObjectMapper mapper = new ObjectMapper();
			File file = new File(cityOutPutFolder + count + ".json");
			file.createNewFile();
			mapper.writeValue(file, hotelList);
			System.out.println("Data exported to file --> " + file.getAbsolutePath());
			count++;
		}
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		WoocationHotelCrawler crawler = new WoocationHotelCrawler();
	}
}
