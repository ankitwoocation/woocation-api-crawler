package com.woocation.crawler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.BeanUtil;
import com.woocation.event.model.Event;
import com.woocation.event.model.EventRaw;
import com.woocation.event.model.GeoLocation;
import com.woocation.utils.EntityToDTO;

/**
 * The Class EventCrawler.
 */
public class EventCrawler {

	/** The Constant DIRECTORY. */
	public static final String DIRECTORY = "E:\\";

	/** The file path. */
	private String filePath = DIRECTORY + "Data\\Event\\";

	private String cityOutPutFinal = DIRECTORY + "Data\\Event\\";

	/** The event list. */
	private List<Event> eventList = new ArrayList<>();

	/**
	 * Instantiates a new event crawler.
	 */
	public EventCrawler() {
		readEventFile();
		writeData();
	}

	/**
	 * Read event file.
	 */
	public void readEventFile() {
		List<EventRaw> eventRawList = readFile(filePath + "songkick.json", EventRaw.class);
		eventRawList.stream().forEach(item -> {
			if(CollectionUtils.isNotEmpty(item.getLdp())){
				GeoLocation location = item.getLdp().get(0).getLocation().getGeo();
				Event event = EntityToDTO.getInstance().getObject(item, EventRaw.class, Event.class);
				event.setLocation(location);
				eventList.add(event);
			}
		});
	}

	/**
	 * Read file.
	 *
	 * @param <T>
	 *            the generic type
	 * @param fileName
	 *            the file name
	 * @param pojoClass
	 *            the pojo class
	 * @return the list
	 */
	public <T> List<T> readFile(String fileName, Class<T> pojoClass) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		List<T> list = null;
		try {
			File jsonFile = new File(fileName);
			list = mapper.readValue(jsonFile, mapper.getTypeFactory().constructCollectionType(List.class, pojoClass));
			System.out.println("Records Read in File --> " + fileName + " --> " + list.size());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

	/**
	 * Write data.
	 *
	 * @param count
	 *            the count
	 */
	private void writeData() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			File file = new File(filePath + "Events_New" + ".json");
			file.createNewFile();
			mapper.writeValue(file, eventList);
			System.out.println("Events exported to file --> " + file.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		EventCrawler eventCrawler = new EventCrawler();
	}

}
