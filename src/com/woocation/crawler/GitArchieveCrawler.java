/**
 * 
 */
package com.woocation.crawler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

import com.google.common.io.ByteStreams;

/**
 * @author admin
 *
 */
public class GitArchieveCrawler {

	private String basePath = "http://data.gharchive.org";

	private static String folderPath = "E:\\";

	/**
	 * 
	 */
	public GitArchieveCrawler() {

	}

	public void downloadData(String date) {
		for (int i = 0; i < 24; i++) {
			String fileName = date + "-" + i + ".json.gz";
			String filePath = basePath + "/" + fileName;
			downloadData(filePath, fileName);
		}
	}

	private void unzip(String zipFilePath, String destDir) {
		try {
			List<File> filesInFolder = Files.walk(Paths.get(zipFilePath)).filter(Files::isRegularFile).map(Path::toFile)
					.collect(Collectors.toList());
			filesInFolder.stream().forEach(file -> {
//				unzipFile(file, destDir);
				gunzipIt(file, destDir);
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * GunZip it
	 */
	public void gunzipIt(File file, String destDir) {
		byte[] buffer = new byte[1024];
		try {
			GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(file));
			String fileName = file.getName().replace(".gz", "");
			File newFile = new File(destDir + File.separator + fileName);
			System.out.println("Unzipping to " + newFile.getAbsolutePath());
			new File(newFile.getParent()).mkdirs();
			FileOutputStream out = new FileOutputStream(fileName);
			int len;
			while ((len = gzis.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}
			gzis.close();
			out.close();
			System.out.println("Done");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	private void unzipFile(File file, String destDir) {
		ZipInputStream zis = null;
		try {
			zis = new ZipInputStream(new FileInputStream(file));
			String fileName = file.getName().replace(".gz", "");
			File newFile = new File(destDir + File.separator + fileName);
			System.out.println("Unzipping to " + newFile.getAbsolutePath());
			new File(newFile.getParent()).mkdirs();
			FileOutputStream fos = new FileOutputStream(newFile);
			byte[] buffer = new byte[1024];
			int len;
			while ((len = zis.read(buffer)) != -1) {
				fos.write(buffer, 0, len);
			}
			fos.close();
			zis.closeEntry();
			zis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void downloadData(String filePath, String fileName) {
		InputStream inputStream = null;
		FileOutputStream out = null;
		try {
			URL url = new URL(filePath);
			URLConnection connection = url.openConnection();
			connection.addRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0");
			connection.setConnectTimeout(10000);
			connection.setReadTimeout(10000);
			connection.setDoOutput(true);
			System.out.println(connection.getContentEncoding());
			inputStream = connection.getInputStream();
			byte[] fileBytes = ByteStreams.toByteArray(inputStream);
			System.out.println(fileBytes);

			out = new FileOutputStream("Download" + "//" + fileName);
			out.write(fileBytes);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GitArchieveCrawler gs = new GitArchieveCrawler();
		// gs.downloadData("2018-01-01");
		gs.unzip("E:\\Download", "Download_Unzip");
	}

}
