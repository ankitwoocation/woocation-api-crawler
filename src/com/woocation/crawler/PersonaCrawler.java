/**
 * 
 */
package com.woocation.crawler;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Splitter;
import com.woocation.hotel.model.PersonaAmenityMapDTO;
import com.woocation.hotel.model.PersonaDTO;
import com.woocation.hotel.model.PersonaLine;

/**
 * The Class PersonaCrawler.
 *
 * @author ankit.gupta
 */
public class PersonaCrawler {

	/** The Constant DIRECTORY. */
	public static final String DIRECTORY = "E:\\";

	/** The file path. */
	private String filePath = DIRECTORY + "Data\\";

	/** The amenity list. */
	private List<PersonaDTO> personaList = new ArrayList<>();

	/** The persona map. */
	private Map<String, PersonaDTO> personaMap = new HashMap<>();

	/** The persona index. */
	private Map<Integer, String> personaIndex = new HashMap<>();

	/**
	 * Instantiates a new persona crawler.
	 */
	public PersonaCrawler() {
		try {
			// readPersonaFile();
			loadAirtablePersona();
			writePersona();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load airtable persona.
	 */
	public void loadAirtablePersona() {
		try {
			List<String> allLines = Files.readAllLines(Paths.get(filePath + "Airtable.csv"));
			readHeader(allLines.get(0));
			allLines.remove(0);// Removing Header
			List<PersonaLine> personaList = allLines.stream().map(line -> PersonaLine.personaBuilder(line))
					.collect(Collectors.toList());
			System.out.println("Records Read in File --> amenity --> " + personaList.size());
			processPersonaList(personaList);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Process persona list.
	 *
	 * @param personaList
	 *            the persona list
	 */
	private void processPersonaList(List<PersonaLine> personaList) {
		personaList.stream().forEach(persona -> {
			List<String> personaValues = persona.getPersonaList();
			for (int i = 8; i < personaValues.size(); i++) {
				String personaRank = personaValues.get(i);
				if (StringUtils.isNotEmpty(personaRank)) {
					String personaName = personaIndex.get(i);
					PersonaDTO personaDto = personaMap.get(personaName);
					if(personaDto == null){
						personaDto = new PersonaDTO();
						personaDto.setPersonaId(personaName);
						personaDto.setPersonaLabel(personaName);
						PersonaAmenityMapDTO amenityDto = new PersonaAmenityMapDTO();
						amenityDto.setAmenityId(persona.getAmenityId());
						amenityDto.setAmenityLabel(persona.getAmenityName());
						amenityDto.setRank(Integer.valueOf(personaRank));
						personaDto.getAmenitiesPersona().add(amenityDto);
					}else{
						PersonaAmenityMapDTO amenityDto = new PersonaAmenityMapDTO();
						amenityDto.setAmenityId(persona.getAmenityId());
						amenityDto.setAmenityLabel(persona.getAmenityName());
						amenityDto.setRank(Integer.valueOf(personaRank));
						personaDto.getAmenitiesPersona().add(amenityDto);
					}
					personaMap.put(personaName, personaDto);
				}
			}
		});
		System.out.println("Total Persona mapped --> " + personaMap.size());
	}

	/**
	 * Read header.
	 *
	 * @param line
	 *            the line
	 */
	private void readHeader(String line) {
		List<String> allHeaders = Splitter.on(",").trimResults().splitToList(line);
		for (int i = 8; i < allHeaders.size(); i++) {
			personaIndex.put(i, allHeaders.get(i));
		}
		System.out.println("Persona found --> " + personaIndex.size());
	}

	/**
	 * Read persona file.
	 */
	public void readPersonaFile() {
		try {
			List<File> filesInFolder = Files.walk(Paths.get(filePath + "Persona")).filter(Files::isRegularFile)
					.map(Path::toFile).collect(Collectors.toList());
			filesInFolder.stream().forEach(file -> {
				PersonaDTO personaDto = new PersonaDTO();
				try {
					List<String> allData = Files.readAllLines(Paths.get(file.getAbsolutePath()));
					personaDto.setPersonaId(FilenameUtils.removeExtension(file.getName()));
					personaDto.setPersonaLabel(personaDto.getPersonaId());
					List<PersonaAmenityMapDTO> amenityList = new ArrayList<>();
					for (String line : allData) {
						List<String> dataLineList = Splitter.on(",").splitToList(line);
						PersonaAmenityMapDTO amenityDto = new PersonaAmenityMapDTO();
						amenityDto.setAmenityId(dataLineList.get(0));
						amenityDto.setAmenityLabel(dataLineList.get(1));
						amenityDto.setRank(Integer.valueOf(dataLineList.get(2)));
						amenityList.add(amenityDto);
					}
					personaDto.setAmenitiesPersona(amenityList);
				} catch (Exception e) {
					e.printStackTrace();
				}
				personaList.add(personaDto);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Records Read in File -->  Persona   --> " + personaList.size());
	}

	/**
	 * Write persona.
	 *
	 * @throws Exception
	 *             the exception
	 */
	private void writePersona() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		File file = new File(filePath + "Persona" + ".json");
		file.createNewFile();
		mapper.writeValue(file, personaMap.values());
		System.out.println("Amenity exported to file --> " + file.getAbsolutePath());

	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		PersonaCrawler crawler = new PersonaCrawler();
	}

}
