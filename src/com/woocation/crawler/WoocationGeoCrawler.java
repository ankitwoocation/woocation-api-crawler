/**
 * 
 */
package com.woocation.crawler;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.woocation.model.Airport;
import com.woocation.model.AirportDetails;
import com.woocation.model.Attraction;
import com.woocation.model.AttractionRaw;
import com.woocation.model.CityEsBean;
import com.woocation.model.Cuisine;
import com.woocation.model.Elevation;
import com.woocation.model.Holiday;
import com.woocation.model.HolidayRaw;
import com.woocation.model.HolidayRawReference;
import com.woocation.model.Languages;
import com.woocation.model.MountainRiverBeach;
import com.woocation.model.Network;
import com.woocation.model.Population;
import com.woocation.model.SafetyReference;
import com.woocation.model.Subway;
import com.woocation.model.UVBean;
import com.woocation.model.Vegetation;
import com.woocation.model.Weather;
import com.woocation.reader.crawler.GeoCityCrawlerJson;

/**
 * The Class WoocationGeoCrawler.
 *
 * @author Ankit.Gupta
 */
public class WoocationGeoCrawler {

	public static final String DIRECTORY = "D:\\";

	/** The city out put. */
	private String cityOutPut = DIRECTORY + "Data\\CityBean\\";

	private String cityOutPutFinal = DIRECTORY + "Data\\CityBeanFinal\\";

	/** The file path. */
	private String filePath = DIRECTORY + "Data\\";

	/** The city file. */
	private String cityFile = DIRECTORY + "Data\\cities_new.txt";

	/** The city export location. */
	private String cityExportLocation = DIRECTORY + "Data\\CityBean\\";

	/** The subway map. */
	public Map<Long, Subway> subwayMap = new HashMap<>();

	/** The elevation map. */
	public Map<Long, Elevation> elevationMap = new HashMap<>();

	/** The population map. */
	public Map<Long, Population> populationMap = new HashMap<>();

	/** The network map. */
	public Map<Long, Network> networkMap = new HashMap<>();

	/** The language map. */
	public Map<Long, Languages> languageMap = new HashMap<>();

	/** The uv map. */
	public Map<Long, UVBean> uvMap = new HashMap<>();

	/** The vegetaion. */
	public Map<Long, Vegetation> vegetationMap = new HashMap<>();

	/** The weather map. */
	public Map<Long, Weather> weatherMap = new HashMap<>();

	/** The holiday map. */
	public Map<Long, List<Holiday>> holidayMap = new HashMap<>();

	/** The city crawler. */
	private GeoCityCrawlerJson cityCrawler = new GeoCityCrawlerJson();

	/** The mrb map. */
	private Map<Long, MountainRiverBeach> mrbMap = new HashMap<>();

	/** The airport map. */
	private Map<Long, List<Airport>> airportMap = new HashMap<>();

	/** The country holiday map. */
	private Map<String, String> workingDayMap = new HashMap<>();

	/** The weather map. */
	public Map<Long, SafetyReference> safetyMap = new HashMap<>();

	/** The cab map. */
	public Map<String, List<String>> cabMap = new HashMap<>();

	/** The threat map. */
	public Map<String, Double> threatMap = new HashMap<>();

	/** The google subway map. */
	public Map<Long, Boolean> googleSubwayMap = new HashMap<>();

	/** The network operator map. */
	public Map<Long, List<String>> networkOperatorMap = new HashMap<>();

	/** The cuisine map. */
	private Map<Long, Cuisine> cuisineMap = new HashMap<>();
	
	/** The country attraction map. */
	private Map<String, List<Attraction>> countryAttractionMap = new HashMap<>();
	
	/** The city attraction map. */
	private Map<Long, List<Attraction>> cityAttractionMap = new HashMap<>();

	/**
	 * Instantiates a new woocation geo crawler.
	 */
	public WoocationGeoCrawler() {
		processRawData();
//		 processSpecificData();
	}

	/**
	 * Process raw data.
	 */
	private void processRawData() {
		try {
			cityCrawler.getGeoCityListGeneric(cityFile);
			readAllDate();
			combinedData();
			clearExistingData();
			writeCityBean(cityOutPut);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Process specific data.
	 */
	private void processSpecificData() {
		try {
			readHolidayFile();
			List<File> filesInFolder = Files.walk(Paths.get(cityOutPut)).filter(Files::isRegularFile).map(Path::toFile)
					.collect(Collectors.toList());
			int count = 1;
			for (File file : filesInFolder) {
				cityCrawler.readGeoCityExistingData(file.getAbsolutePath());
				combinedHolidayData();
				writeData(count);
				cityCrawler.clearList();
				count++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write data.
	 *
	 * @param count
	 *            the count
	 */
	private void writeData(int count) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			File file = new File(cityOutPutFinal + count + ".json");
			file.createNewFile();
			mapper.writeValue(file, cityCrawler.getCityList());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write city bean.
	 *
	 * @throws Exception
	 *             the exception
	 */
	private void writeCityBean(String cityOutPutFolder) throws Exception {
		List<List<CityEsBean>> subSets = Lists.partition(cityCrawler.getCityList(), 1000);
		int count = 1;
		for (List<CityEsBean> cityList : subSets) {
			ObjectMapper mapper = new ObjectMapper();
			File file = new File(cityOutPutFolder + count + ".json");
			file.createNewFile();
			mapper.writeValue(file, cityList);
			System.out.println("Data exported to file --> " + file.getAbsolutePath());
			count++;
		}
	}

	/**
	 * Clear existing data.
	 *
	 * @throws Exception
	 *             the exception
	 */
	private void clearExistingData() throws Exception {
		File file = new File(cityExportLocation);
		if (file.isDirectory()) {
			List<File> filesInFolder = Files.walk(Paths.get(cityExportLocation)).filter(Files::isRegularFile)
					.map(Path::toFile).collect(Collectors.toList());

			filesInFolder.stream().forEach(prevFile -> prevFile.delete());
		}
	}

	/**
	 * Combined data.
	 */
	public void combinedData() {
		for (CityEsBean cityBean : cityCrawler.getCityList()) {
			try {
				Long geoNameId = cityBean.getGeonameId();

				Subway subway = subwayMap.get(geoNameId);
				cityBean.setSubway(subway);

				Elevation elevationRef = elevationMap.get(geoNameId);
				cityBean.setElevationRef(elevationRef);

				Population populationRef = populationMap.get(geoNameId);
				cityBean.setPopulationRef(populationRef);

				Network network = networkMap.get(geoNameId);
				cityBean.setNetwork(network);

				Languages languagesRef = languageMap.get(geoNameId);
				cityBean.setLanguagesRef(languagesRef);

				UVBean uvDetails = uvMap.get(geoNameId);
				cityBean.setUvDetails(uvDetails);

				Vegetation vegetation = vegetationMap.get(geoNameId);
				cityBean.setVegetation(vegetation);

				Weather weather = weatherMap.get(geoNameId);
				cityBean.setWeather(weather);

				List<Airport> airportList = airportMap.get(geoNameId);
				cityBean.setAirports(airportList);

				MountainRiverBeach mrb = mrbMap.get(geoNameId);
				if (mrb != null) {
					cityBean.setMountain(mrb.isMountain());
					cityBean.setRiver(mrb.isRiver());
					cityBean.setBeach(mrb.isBeach());
				}

				String countryCode = cityBean.getCountryCode();
				String workingDay = workingDayMap.get(countryCode);
				if (workingDay != null) {
					cityBean.setWorkingDays(workingDay);
				} else {
					cityBean.setWorkingDays("NA");
				}

				SafetyReference reference = safetyMap.get(geoNameId);
				cityBean.setSafetyReference(reference);

				List<String> cabDetials = cabMap.get(String.valueOf(geoNameId));
				cityBean.setCabDetails(cabDetials);

				Double threatCount = threatMap.get(cityBean.getCountryName().toUpperCase());
				if (threatCount != null && threatCount > 0.0d) {
					cityBean.setThreatRatio(threatCount);
				}

				Boolean subwayAvailable = googleSubwayMap.get(geoNameId);
				cityBean.setGoogleSubway(subwayAvailable);

				List<String> networkDetails = networkOperatorMap.get(String.valueOf(geoNameId));
				cityBean.setNetworkOperator(networkDetails);

				Cuisine cusine = cuisineMap.get(geoNameId);
				if (cusine != null) {
					cityBean.setCuisines(cusine.getCuisines());
				}
				
				List<Attraction> attractionList = countryAttractionMap.get(cityBean.getCountryCode());
				if(attractionList != null && attractionList.size() > 5){
					cityBean.setCountryAttractionList(attractionList.subList(0, 5));
				}else{
					cityBean.setCountryAttractionList(attractionList);
				}
				
				List<Attraction> cityAttractionList = cityAttractionMap.get(geoNameId);
				if(cityAttractionList != null && cityAttractionList.size() > 5){
					cityBean.setCityAttractionList(cityAttractionList.subList(0, 5));
				}else{
					cityBean.setCityAttractionList(cityAttractionList);
				}
				
			} catch (Exception e) {
				System.out.println(e.getMessage() + "  --> " + cityBean.getGeonameId());
			}
		}
		System.out.println(cityCrawler.getCityList().get(0));
	}

	/**
	 * Combined holiday data.
	 */
	public void combinedHolidayData() {
		for (CityEsBean cityBean : cityCrawler.getCityList()) {
			Long geoNameId = cityBean.getGeonameId();
			List<Holiday> holidayList = holidayMap.get(geoNameId);
			cityBean.setHolidays(holidayList);
		}
	}

	/**
	 * Read all date.
	 */
	private void readAllDate() {
		readSubwayFile();
		readElevationFile();
		readPopulationFile();
		readNetworkFile();
		readLanguageFile();
		readUVFile();
		readWeatherFile();
		readVegetationFile();
		readMountainRiverBeachData();
		readAirportFile();
		readWorkingDayData();
		readCitySafetyFile();
		readCabFile();
		readThreatData();
		readGoogleSubwayAvailableData();
		readNetworkConnectionAvailableData();
		readCuisineData();
		readCounryAttractionData();
		readCityAttractionData();
	}

	private void readCityAttractionData() {
		String attractionFile = filePath + "Topattraction_city.json";
		try {
			List<AttractionRaw> list = readFile(attractionFile, AttractionRaw.class);
			list.stream().forEach(item -> {
				cityAttractionMap.put(item.getGeonameid(), item.getAttractions());
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Read counry attraction data.
	 */
	private void readCounryAttractionData() {
		String attractionFile = filePath + "Topattraction_country.json";
		try {
			List<AttractionRaw> list = readFile(attractionFile, AttractionRaw.class);
			list.stream().forEach(item -> {
				countryAttractionMap.put(item.getCountry(), item.getAttractions());
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Read cuisine data.
	 */
	private void readCuisineData() {
		String cuisineFile = filePath + "Cuisine.json";
		try {
			List<Cuisine> list = readFile(cuisineFile, Cuisine.class);
			list.stream().forEach(item -> {
				cuisineMap.put(Long.valueOf(item.getGeonameid()), item);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read network connection available data.
	 */
	private void readNetworkConnectionAvailableData() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		String networkProvider = filePath + "network_provider.json";
		try {
			File jsonFile = new File(networkProvider);
			networkOperatorMap = mapper.readValue(jsonFile, new TypeReference<Map<String, List<String>>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read google subway available data.
	 */
	private void readGoogleSubwayAvailableData() {
		String threatFile = filePath + "google_subway.csv";
		try {
			File jsonFile = new File(threatFile);
			List<String> allLines = Files.readAllLines(jsonFile.toPath());
			allLines.stream().forEach(line -> {
				String[] items = line.split(",");
				googleSubwayMap.put(Long.valueOf(items[0]), Boolean.valueOf(items[5]));
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read threat data.
	 */
	private void readThreatData() {
		String threatFile = filePath + "Threat.txt";
		try {
			File jsonFile = new File(threatFile);
			List<String> allLines = Files.readAllLines(jsonFile.toPath());
			allLines.stream().forEach(line -> {
				String[] items = line.split(",");
				threatMap.put(items[0].toUpperCase(), Double.valueOf(items[1]));
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read subway file.
	 */
	public void readCabFile() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		String cabFile = filePath + "Cabs.json";
		try {
			File jsonFile = new File(cabFile);
			cabMap = mapper.readValue(jsonFile, new TypeReference<Map<String, List<String>>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read subway file.
	 */
	public void readSubwayFile() {
		String subwayFile = filePath + "subway_geonameid.json";
		try {
			List<Subway> list = readFile(subwayFile, Subway.class);
			list.stream().forEach(item -> {
				subwayMap.put(Long.valueOf(item.getGeonameid()), item);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read Elevation file.
	 */
	public void readElevationFile() {
		String elevationFile = filePath + "elevation_geonameid.json";
		try {
			List<Elevation> list = readFile(elevationFile, Elevation.class);
			list.stream().forEach(item -> {
				elevationMap.put(Long.valueOf(item.getGeonameId()), item);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void readCitySafetyFile() {
		String safetyFile = filePath + "citySafety.json";
		try {
			List<SafetyReference> list = readFile(safetyFile, SafetyReference.class);
			list.stream().forEach(item -> {
				safetyMap.put(item.getGeonameid(), item);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read population file.
	 */
	public void readPopulationFile() {
		List<Population> populationList = readFile(filePath + "population_geonameid.json", Population.class);
		populationList.stream().forEach(item -> {
			populationMap.put(Long.valueOf(item.getGeonameid()), item);
		});
	}

	/**
	 * Read weather file.
	 */
	public void readWeatherFile() {
		try {
			List<File> filesInFolder = Files.walk(Paths.get(filePath + "Weather")).filter(Files::isRegularFile)
					.map(Path::toFile).collect(Collectors.toList());
			filesInFolder.stream().forEach(file -> {
				List<Weather> weatherList = readFile(file.getAbsolutePath(), Weather.class);
				weatherList.stream().forEach(item -> {
					weatherMap.put(Long.valueOf(item.getGeonameid()), item);
				});
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read weather file.
	 */
	public void readHolidayFile() {
		try {
			List<File> filesInFolder = Files.walk(Paths.get(filePath + "Holidays")).filter(Files::isRegularFile)
					.map(Path::toFile).collect(Collectors.toList());
			filesInFolder.stream().forEach(file -> {
				List<HolidayRawReference> holidayList = readFile(file.getAbsolutePath(), HolidayRawReference.class);
				holidayList.stream().forEach(item -> {
					holidayMap.put(Long.valueOf(item.getGeonameid()), getHoliday(item.getHolidays()));
				});
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Records Read in File -->  holidayMap   --> " + holidayMap.size());
	}

	private List<Holiday> getHoliday(List<HolidayRaw> rawList) {
		List<Holiday> holidayList = new ArrayList<>();
		for (HolidayRaw rawObject : rawList) {
			Holiday holiday = new Holiday();
			String date = rawObject.getDd() + "-" + rawObject.getMm() + "-" + rawObject.getYy();
			holiday.setHolidayName(rawObject.getName());
			holiday.setHolidayDate(date);
			holidayList.add(holiday);
		}
		return holidayList;
	}

	/**
	 * Read mountain river beach data.
	 */
	public void readMountainRiverBeachData() {
		try {
			List<String> allLines = Files.readAllLines(Paths.get(filePath + "mountain_river_beach.csv"));
			allLines.stream().forEach(line -> {
				List<String> dataLineList = Splitter.on(",").splitToList(line);
				MountainRiverBeach reference = new MountainRiverBeach(dataLineList.get(0), dataLineList.get(1),
						dataLineList.get(2), dataLineList.get(3));
				mrbMap.put(Long.valueOf(reference.getGeoNameId()), reference);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Records Read in File -->  mountain_river_beach   --> " + mrbMap.size());
	}

	/**
	 * Read network file.
	 */
	public void readNetworkFile() {
		List<Network> networkList = readFile(filePath + "network_geonameid.json", Network.class);
		networkList.stream().forEach(item -> {
			networkMap.put(Long.valueOf(item.getGeonameid()), item);
		});
	}

	/**
	 * Read network file.
	 */
	public void readAirportFile() {
		List<AirportDetails> airPortList = readFile(filePath + "airports.json", AirportDetails.class);
		airPortList.stream().forEach(item -> {
			airportMap.put(Long.valueOf(item.getGeonameid()), item.getAirports());
		});
	}

	/**
	 * Read language file.
	 */
	public void readLanguageFile() {
		List<Languages> languageList = readFile(filePath + "languages_geoname_new.json", Languages.class);
		languageList.stream().forEach(item -> {
			languageMap.put(item.getGeonameid(), item);
		});
	}

	/**
	 * Read vegetation file.
	 */
	public void readVegetationFile() {
		List<Vegetation> vegetationList = readFile(filePath + "vegetation_geonameid.json", Vegetation.class);
		vegetationList.stream().forEach(item -> {
			vegetationMap.put(Long.valueOf(item.getGeonameid()), item);
		});
	}

	/**
	 * Read UV file.
	 */
	public void readUVFile() {
		List<UVBean> uvList = readFile(filePath + "uvi_geonameid.json", UVBean.class);
		uvList.stream().forEach(item -> {
			uvMap.put(Long.valueOf(item.getGeonameid()), item);
		});
	}

	private void readWorkingDayData() {
		try {
			List<String> data = Files.readAllLines(Paths.get(filePath + "holidays_country.csv"));
			for (String line : data) {
				String[] lineData = line.split(",");
				workingDayMap.put(lineData[1], lineData[3].toUpperCase());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Read file.
	 *
	 * @param <T>
	 *            the generic type
	 * @param fileName
	 *            the file name
	 * @param pojoClass
	 *            the pojo class
	 * @return the list
	 */
	public <T> List<T> readFile(String fileName, Class<T> pojoClass) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
		List<T> list = null;
		try {
			File jsonFile = new File(fileName);
			list = mapper.readValue(jsonFile, mapper.getTypeFactory().constructCollectionType(List.class, pojoClass));
			System.out.println("Records Read in File --> " + fileName + " --> " + list.size());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;

	}

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		WoocationGeoCrawler crawler = new WoocationGeoCrawler();
	}

}
