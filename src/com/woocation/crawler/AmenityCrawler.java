/**
 * 
 */
package com.woocation.crawler;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Splitter;
import com.woocation.hotel.model.AmenityDTO;

/**
 * @author ankit.gupta
 *
 */
public class AmenityCrawler {

	/** The Constant DIRECTORY. */
	public static final String DIRECTORY = "D:\\";

	/** The file path. */
	private String filePath = DIRECTORY + "Data\\";

	/** The amenity list. */
	private List<AmenityDTO> amenityList = new ArrayList<>();

	/**
	 * 
	 */
	public AmenityCrawler() {
		try {
			readAmenityData();
			writeAmenity();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void readAmenityData() {
		try {
			List<String> allLines = Files.readAllLines(Paths.get(filePath + "Airtable.csv"));
			allLines.remove(0); // Removing Header
			allLines.stream().forEach(line -> {
				List<String> dataLineList = Splitter.on(",").splitToList(line);
				AmenityDTO amenity = new AmenityDTO();
				amenity.setAmenityId(dataLineList.get(0));
				amenity.setAmenityLabel(dataLineList.get(1));
				amenity.setAmenityAlternateLabel(dataLineList.get(2));
				amenity.setCategory(dataLineList.get(6));
				String defaultRank = dataLineList.get(7);
				amenity.setDefaultRanking(StringUtils.isNotEmpty(defaultRank) ? Integer.valueOf(defaultRank) : null);
				amenityList.add(amenity);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Records Read in File -->  amenity   --> " + amenityList.size());
	}

	private void writeAmenity() throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		File file = new File(filePath + "Amenity_new1" + ".json");
		file.createNewFile();
		mapper.writeValue(file, amenityList);
		System.out.println("Amenity exported to file --> " + file.getAbsolutePath());

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AmenityCrawler crawler = new AmenityCrawler();
	}

}
